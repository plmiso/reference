package com.company;

import javax.print.DocFlavor;
import java.util.List;
import java.util.Optional;
import java.util.logging.Logger;
import java.util.stream.Collectors;

public class HotelService {

    private Hotel hotel;

    private Logger LOG;


    private static final String BOOKING_IMPOSIBLE = "Room cannot be booked: ";
    private static final String ROOM_BOOKED_SUCESSFULLY = "Room booked sucessfully : ";
    private static final String ROOM_FREED = "Room sucesfully freed : ";
    private static final String ROOM_ALREADY_AVALIABLE = "Room already avaliable";

    public HotelService(Hotel hotel) {
        LOG = Logger.getLogger(HotelService.class.getName());
        this.hotel = hotel;
    }

    public List<Room> getListOfRooms() {
        return this.hotel.getListOfRooms();
    }

    public List<Room> getLisOfAvaliableRooms() {
        return getListOfRooms().stream().
                filter(room -> room.isRoomAvaliable()).
                collect(Collectors.toList());
    }

    public boolean bookRoom(int roomNumber) {
        Optional<Room> room = getListOfRooms().stream().
                filter(room1 -> room1.getRoomNumber() == roomNumber && room1.isRoomAvaliable()).
                findFirst();
        if (room.isPresent()) {
            room.get().setRoomAvaliable(false);
            LOG.info( ROOM_BOOKED_SUCESSFULLY + roomNumber);
            return true;

        } else {
            LOG.info( BOOKING_IMPOSIBLE + roomNumber);
            return false;
        }
    }

    public boolean freeRoom(int roomNumber) {
        Optional<Room> room = getListOfRooms().stream().
                filter(room1 -> room1.getRoomNumber() == roomNumber && !room1.isRoomAvaliable()).
                findFirst();
        if (room.isPresent()) {
            LOG.info( ROOM_FREED + roomNumber);
            room.get().setRoomAvaliable(true);
        } else {
            LOG.info( ROOM_ALREADY_AVALIABLE + roomNumber);
        }
        return true;
    }

}
