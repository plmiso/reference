package com.company;

public class Room {

    private int roomNumber;
    private int roomSize;
    private boolean roomHasBathroom;
    private boolean isRoomAvaliable;

    public Room(int roomNumber, int roomSize, boolean roomHasBathroom, boolean isRoomAvaliable){
        this.roomNumber = roomNumber;
        this.roomSize = roomSize;
        this.roomHasBathroom = roomHasBathroom;
        this. isRoomAvaliable = isRoomAvaliable;
    }

    public int getRoomNumber() {
        return roomNumber;
    }

    public void setRoomNumber(int roomNumber) {
        this.roomNumber = roomNumber;
    }

    public int getRoomSize() {
        return roomSize;
    }

    public void setRoomSize(int roomSize) {
        this.roomSize = roomSize;
    }

    public boolean isRoomHasBathroom() {
        return roomHasBathroom;
    }

    public void setRoomHasBathroom(boolean roomHasBathroom) {
        this.roomHasBathroom = roomHasBathroom;
    }

    public boolean isRoomAvaliable() {
        return isRoomAvaliable;
    }

    public void setRoomAvaliable(boolean roomAvaliable) {
        isRoomAvaliable = roomAvaliable;
    }
}
