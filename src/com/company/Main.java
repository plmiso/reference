package com.company;

import java.util.Scanner;
import java.util.logging.Level;
import java.util.logging.Logger;

public class Main {

    public static Hotel hotel;
    public static Logger LOG;

    public static void main(String[] args) {

        buildItAll();
        HotelService service = new HotelService(hotel);
        service.bookRoom(44);
        service.bookRoom(44);
        service.freeRoom(44);
        service.freeRoom(44);


    }

    public static void buildItAll(){
        LOG = Logger.getLogger(Main.class.getName());
        hotel = new Hotel();
    }

    public static void displayMenu(){
        Scanner scanner = new Scanner(System.in);
        do {
            System.out.println("\n\n          Hotel Service Menu");
            System.out.println("--------------------------------------");
            System.out.println("1 - ");
            System.out.println("2 - ");
            System.out.println("3 - ");
            System.out.println("4 - ");
            System.out.println("5 - ");
            System.out.println("6 - ");
            System.out.println("7 - ");
            System.out.println("8 - ");
            System.out.println("9 - ");
            System.out.print("\nSelect a Menu Option: ");
            try {
                int input = scanner.nextInt(); // Get user input from the keyboard


                switch (input) {
                    case 1:  // do something
                        break;
                    case 2:  // do something
                        break;
                    default:
                        LOG.log(Level.SEVERE, "Unrecoginzed input");
                        System.out.println("Unrecognized input, please try again");
                }
            } catch (NumberFormatException e) {
                LOG.log(Level.SEVERE, "User provided non digit character: " + e);
            }

        }
        while(true); // Display the menu until the user closes the program

    }
}
