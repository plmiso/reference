package com.company;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class Hotel {

    private List<Room> listOfRooms;


    public Hotel(){
        listOfRooms = new ArrayList<>();
        Random random = new Random();
        for (int i = 0; i < 10 ; i++) {
            for (int j = 0; j < 10 ; j++) {
                Room  room = new Room(Integer.valueOf(String.valueOf(i) + String.valueOf(j)),
                        random.nextInt((5-1) + 1) +1,
                        random.nextBoolean(), random.nextBoolean());
                listOfRooms.add(room);
            }
        }
    }

    public List<Room> getListOfRooms() {
        return listOfRooms;
    }

    public void setListOfRooms(List<Room> listOfRooms) {
        this.listOfRooms = listOfRooms;
    }
}
